# Use phusion/baseimage as base image. To make your builds reproducible, make
# sure you lock down to a specific version, not to `latest`!
# See https://github.com/phusion/baseimage-docker/blob/master/Changelog.md for
# a list of version numbers.
FROM phusion/baseimage:0.9.22
MAINTAINER Vojtěch Biberle <vojtech.biberle@gmail.com>

ENV WEBAPP_DIR /var/www/website
ENV WEBAPP_USER webapp
ENV WEBAPP_GROUP webapp
ENV WEBAPP_GID 1000
ENV WEBAPP_UID 1000
ENV PHP_FPM_HOST php
ENV PHP_FPM_PORT 9000

ENV APACHE_RUN_USER $WEBAPP_USER
ENV APACHE_RUN_GROUP $WEBAPP_GROUP
ENV APACHE_DOCUMENTROOT $WEBAPP_DIR

# Use baseimage-docker's init system.
CMD ["/sbin/my_init"]

RUN apt-add-repository multiverse && \
    apt-get update && \ 
    apt-get install -y \
    apache2 \
    libapache2-mod-fastcgi

ADD root /


RUN groupadd -g $WEBAPP_GID $WEBAPP_GROUP && \
    useradd -d $WEBAPP_DIR -g $WEBAPP_GID -u $WEBAPP_UID $WEBAPP_USER && \
    chown $WEBAPP_USER:$WEBAPP_GROUP $WEBAPP_DIR && \
    gpasswd -a www-data $WEBAPP_GROUP && \
    a2enmod actions fastcgi alias rewrite && \
    a2enconf php-fpm && \
    a2enmod proxy_fcgi && \
    a2dissite 000-default && \
    a2ensite localhost && \
    a2ensite localhost-ssl

EXPOSE 80 443

# Clean up APT when done.
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
