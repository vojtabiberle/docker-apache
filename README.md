# apache-php7
In this container is added apache2.4 with mpm-event and linked with php7.0-fpm.

## Volumes
One volume $WEBAPP_DIR

## Ports
80 443

virtualhost on port 443 is disabled

## Environment variables

### APACHE_DOCUMENTROOT
Path of apache document root.