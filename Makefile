MAINTAINER_NS = vojtabiberle
NAME = docker-apache
REGISTRY=registry.gitlab.com

files = Dockerfile

build: $(files)
	docker build -t ${REGISTRY}/$(MAINTAINER_NS)/$(NAME) .

run:
	docker run --rm -i -t -p 80:80 -p 443:443  ${REGISTRY}/$(MAINTAINER_NS)/$(NAME) /sbin/my_init -- bash -l

push:
	docker push ${REGISTRY}/$(MAINTAINER_NS)/$(NAME)
